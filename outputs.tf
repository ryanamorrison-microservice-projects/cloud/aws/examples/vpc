# outputs for testing
output "vpc_id" {
  value       = module.example_vpc.vpc_id
  description = "ID of the VPC"
}
output "vpc_tags" {
  value       = module.example_vpc.vpc_tags
  description = "all tags for this VPC"
}
